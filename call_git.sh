#!/bin/sh

# ############################# constantes ################################ #

# Go inside directory
BASEDIR=$(dirname "$0")
cd $BASEDIR

# load variables inside .env file
. ./.env
NAME_APP=$(eval echo $( grep container_name docker-compose.yml | cut -d":" -f2 | sed 's/"//g'))
GITURLAPI="https://framagit.org/api/v4/projects/${GIT_PROJECT_ID}/repository/branches/${GIT_BRANCH}"
LOCKFILE="/tmp/${NAME_APP}.lock"

TSTEP=5    # Time between 2 updates (seconds)
MAXTIME=60 # Time between 2 cron tasks (seconds)


# ############################# parameters ################################ #

# Read arguments :
# (functions from : https://stackoverflow.com/a/50994244 )
usage () {
  cat << EOF

usage: $MYNAME [-hvVx] [-a arg] ...

  Perform nifty operations on objects specified by arguments.

Options:
  -a arg   do something with arg
  -h       display this help text and exit
  -v       verbose mode
  -l       loop on $MAXTIME (s) with a timestep of $TSTEP (s)
  -r       rebuild the Dockerfile

EOF
  exit $1
}

rebuild_docker=""

parse_options () {
  opt_verbose=false
  while getopts :a:hvlr option; do
    case $option in
      (a)  opt_a=$OPTARG;;
      (h)  usage 0;;
      (v)  opt_verbose=true;;
      (l)  COUNTER=$(($MAXTIME/$TSTEP));;
      (r)  rebuild_docker="--no-cache";;
      (?)  usage 1;;
    esac
  done
}


# ############################# functions  ################################ #

calc () {
	awk "BEGIN { print "$*" }";
}

updateDockerContainer () {

     	fileLastCommit="/tmp/bot_${GITID}_${GITBRANCH}.log"

	date_commit=$(curl -s $GITURLAPI | python3 -c "import sys, json; print(json.load(sys.stdin)['commit']['committed_date'])")
	giturl=$(curl -s $GITURLAPI | python3 -c "import sys, json; print(json.load(sys.stdin)['commit']['web_url'])" | cut -d'-' -f1)	

	# Aller chercher la date du dernier commit
	if [ -f $fileLastCommit ] ; then
	    date_previous=$(cat $fileLastCommit )
	    tstamp_previous=$(date -d "$date_previous" +%s)
	else
	    date_previous=0
	    tstamp_previous=0
	fi

	tstamp_commit=$(date -d "$date_commit" +%s)

	id_container=$(docker ps  --filter name=^/${NAME_APP} --format '{{.ID}}')
	if [ $tstamp_commit -gt $tstamp_previous ] || [ ! -z $rebuild_docker ] ; then

	    docker stop $id_container

	    # J'ai juste a rebuilder, tout se passant dans le Docker
	    docker compose build --progress plain --build-arg GIT_PROJECT_ID=${GIT_PROJECT_ID} --build-arg GIT_BRANCH=${GIT_BRANCH}  ${rebuild_docker}  && docker compose up -d
	    wait

	else
	    if [ ! -z $opt_verbose ];then
		    echo "last version builded $date_commit  vs  $date_previous"
	    fi
	    # Demarrer le container s il ne tourne pas
	    if [ -z $id_container ];then
		if [ ! -z $opt_verbose ];then
			echo "START container : $id_container"
		fi
		# test if image already builded :
		image_builded=$(docker images | grep $(basename "$PWD") | cut -d" " -f1)
		if [ -z $image_builded ] ;then
			LOGDIR=log_${GITID}_${GITBRANCH}
			mkdir -p $LOGDIR  && chmod 777 $LOGDIR
			docker compose build --progress plain --build-arg GIT_PROJECT_ID=${GIT_PROJECT_ID} --build-arg GIT_BRANCH=${GIT_BRANCH}  ${rebuild_docker}  
		fi
		docker compose up -d
	        #else
		#echo "UP   : $id_container"
	    fi
	fi
	
	# Ecrire le nouveau timestamp
	echo $date_commit > $fileLastCommit
	rm $LOCKFILE

}

# ########################### start script ################################ #
parse_options "$@"
shift $((OPTIND - 1))


if [ -z $COUNTER ];then
    COUNTER=1
fi

if [ ! -z $opt_verbose ];then
    echo "Start" $COUNTER  $TSTEP $MAXTIME
fi
i=0
while [ $i -lt $COUNTER ]
do
	if [ ! -z opt_verbose ];then
	        echo "$i    ::::   $(date)"
	fi
	if [  -f "$LOCKFILE" ];then
		echo "locked"
	else
		touch $LOCKFILE
		updateDockerContainer &
	fi

	i=$(($i+1))
	sleep $TSTEP
done


if [ ! -z $opt_verbose ];then
   echo "Done"
fi
wait
