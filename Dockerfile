FROM python:3.9-slim as base

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1


FROM base AS bot

# Install pkg -----------
# Install pipenv and compilation dependencies
RUN apt-get update && apt-get install -y --no-install-recommends git
RUN python3 -m pip install --upgrade pip && pip install requests

# Reduce privileges -----
RUN useradd -ms /bin/bash userdocker
USER userdocker
ENV HOME="/home/userdocker/"
WORKDIR $HOME

# Git clone and pull ---
# I have to declare this args but set using the command  --build-arg GIT_BRANCH=1234  for exemple
ARG GIT_PROJECT_ID
ARG GIT_BRANCH
# this dir is only to have a place to clone the git repo, but useless after (datas are moved)
ARG DIR_GIT="git_tmp"

# Careful : here, the git (framagit.org) is a gitlab. For github, you have to use another url.
ENV GITURLAPI="https://framagit.org/api/v4/projects/${GIT_PROJECT_ID}/repository/branches/${GIT_BRANCH}"

# this will copy the last change from your brach and it'll invalidate the cache if there was a new change
ADD $GITURLAPI /tmp/devalidateCache

# the actual clone / pull command : first ; get the git repo url
RUN export giturl=$(python3 -c "import sys, json, requests; print(json.loads( (requests.get('$GITURLAPI')).content )['commit']['web_url']) " | cut -d'-' -f1 ) && \
(git -C ${DIR_GIT} pull --ff-only || ( rm -f ${DIR_GIT} && git clone -b ${GIT_BRANCH} ${giturl} $DIR_GIT && git -C ${DIR_GIT} pull --ff-only ) ) && \
mv ${DIR_GIT}/* ./


# Env Python ------------
# . Install packages with pip
RUN pip install --user -r requirements.txt


# Copy cfg file --------
RUN ln -s /var/local/scriptcfg/cfg

# Run Python -----------
CMD ["python3", "./main.py"]

