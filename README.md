# Usage
1) Rename the file `_.env` into `.env` (so without name, only the extension) and modify the .env file :

- `GIT_PROJECT_ID` : id of the projet (in GitLab project repository main page)
- `GIT_BRANCH` : banch to be clone (main for production, dev for developpment branch, ...)

2) Copy the cfg directory from the project inside this directory (or create an empty dir)

3) change the rights of the directory logs : 777

4-opt) Modify cron : `$ crontab -e` with :
```
*/1 * * * * mkdir -p /work/the/path/log_(git_id_project)_(git_branch) &&  /work/the/path/call_git.sh -l  >> /work/the/path/log_(git_id_project)_(git_branch)/docker.cron.log 2>&1
```

5-opt) Inside the directory, to build and launch now the code : `./call_git -r`

# Help :
if there is some troubles, see logs with `$ docker logs  (id)`
